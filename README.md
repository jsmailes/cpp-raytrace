# cpp-raytrace

An example CPU raytracing renderer written in C++. Written as part of [this blog post](http://joshuasmailes.co.uk/blog/2020/07/08/understanding-raytracing.html)!

![Example Render](/src/render.png)

## Requirements

* `gcc-c++`
* `make`
* `CImg-devel`

## Usage

This renderer was designed as a basic example of path tracing, and cannot import `.obj` files. Check out `main.cpp` for an example of basic usage, including setting up a renderer, adding primitives and lights, and outputting a final render. The result is saved under `render.png`.
