#include "lights.h"

AmbientLight::AmbientLight(const vec3 colour) {
    colour_ = colour;
}

vec3 AmbientLight::illumination(Intersection &in, vec3 &origin) {
    return colour_;
}

PhongLight::PhongLight(const vec3 pos, const vec3 colour) {
    pos_ = pos;
    colour_ = colour;
}

/*
PhongLight::PhongLight(const vec3 pos, const vec3 colour_diffuse, const vec3 colour_specular) {
    pos_ = pos;
    colour_diffuse_ = colour_diffuse;
    colour_specular_ = colour_specular;
}
*/

vec3 PhongLight::illumination(Intersection &in, vec3 &origin) {
    vec3 colour = vec3(0.,0.,0.);
    vec3 N = in.object->normal(in.point);
    vec3 L = (pos_ - in.point).norm();
    vec3 R = N * (2 * N.dot(L)) - L;
    vec3 V = (origin - in.point).norm();
    float diffuse = N.dot(L);
    float specular = R.dot(V);

    if (diffuse < 0)
        diffuse = 0;
    if (specular < 0)
        specular = 0;

    //colour = colour_diffuse_ * (in.object->phong_diffuse * diffuse) + colour_specular_ * (in.object->phong_specular * pow(specular, in.object->phong_exponent));
    colour = colour_ * (in.object->phong_diffuse * diffuse + in.object->phong_specular * pow(specular, in.object->phong_exponent));

    return colour;
}

DirectionalLight::DirectionalLight(vec3 dir_, const vec3 colour_) {
    dir = dir_.norm();
    colour = colour_;
}