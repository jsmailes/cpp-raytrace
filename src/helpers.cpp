#include "helpers.h"

Colour::Colour(unsigned char red, unsigned char green, unsigned char blue) {
    r = red;
    g = green;
    b = blue;
}

Colour::Colour(const Colour &col) {
    r = col.r;
    g = col.g;
    b = col.b;
}

Colour Colour::operator+(const Colour &col) {
    unsigned char r = (col.r > 255-r) ? 255 : r+col.r;
    unsigned char g = (col.g > 255-g) ? 255 : g+col.g;
    unsigned char b = (col.b > 255-b) ? 255 : b+col.b;
    return Colour(r+col.r, g+col.g, b+col.b);
    //return Colour(r+col.r, g+col.g, b+col.b);
}

Colour &Colour::operator+=(const Colour &col) {
    r = (col.r > 255-r) ? 255 : r+col.r;
    g = (col.g > 255-g) ? 255 : g+col.g;
    b = (col.b > 255-b) ? 255 : b+col.b;
    return *this;
}

float random01() {
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

vec3 spherical_to_vector(vec3 normal, float theta, float phi) {
    vec3 tangent1 = normal.cross(vec3(1,0,0)).norm();
    if (tangent1.magnitude() < EPSILON) {
        tangent1 = normal.cross(vec3(0,1,0)).norm();
    }
    vec3 tangent2 = normal.cross(tangent1).norm();

    //vec3 temp = tangent1 * cos(phi) + tangent2 * sin(phi);
    //float a = tan(theta);
    //vec3 result = (normal + temp*a).norm();
    //return result;

    return (normal + ((tangent1 * cos(phi) + tangent2 * sin(phi)) * tan(theta))).norm();
}

float sample_diffuse1() {
    return acos(sqrt(random01()));
}

float sample_diffuse2() {
    return 2 * M_PI * random01();
}

float sample_specular1(float exponent) {
    return acos(pow(random01(), 1/(exponent+1)));
}

float sample_specular2() {
    return 2 * M_PI * random01();
}

float clamp(float val) {
    return (val < EPSILON) ? 0 : val;
}

/*
vec3 sample_diffuse(vec3 normal) {
    float theta = acos(sqrt(random01()));
    float phi = 2 * M_PI * random01();
    return spherical_to_vector(normal, theta, phi);
}

vec3 sample_specular(vec3 reflected, float exponent) {
    float alpha = acos(pow(random01(), 1/(exponent+1)));
    float phi = 2 * M_PI * random01();
    return spherical_to_vector(reflected, alpha, phi);
}
*/