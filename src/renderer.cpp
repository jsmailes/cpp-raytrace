#include "renderer.h"

Renderer::Renderer(int width, int height, vec3 camera, vec3 look, vec3 up, float fov, vec3 background) {
    width_ = width;
    height_ = height;
    camera_ = camera;
    look_ = look.norm();
    up_ = up.norm();
    fov_ = fov;
    background_ = background;

    left_ = up.cross(look).norm();
    w_ = h_ = tan(fov/2);
    float ratio = (float)height_ / (float)width_;
    if (ratio < 1.0) {
        h_ *= ratio;
    }
    else {
        w_ /= ratio;
    }
}

void Renderer::add_primitive(Primitive *prim) {
    scene_.push_back(prim);
}

void Renderer::add_light(Light *light) {
    lights_.push_back(light);
}

void Renderer::add_directional_light(DirectionalLight *light) {
    directional_lights_.push_back(light);
}

Intersection Renderer::raycast(vec3 origin, vec3 direction) {
    Intersection result;
    result.t = FLT_MAX;
    result.success = false;
    for (auto const& prim : scene_) {
        float t = prim->intersect(origin, direction);
        if (t > EPSILON && t < result.t) {
            result.object = prim;
            result.t = t;
            result.point = origin + direction*t;
            result.success = true;
        }
    }
    return result;
}

vec3 Renderer::pathtrace(vec3 origin, vec3 direction, int depth, int maxdepth) {
    if (depth >= maxdepth) {
        return vec3(0,0,0); // the ray has bounced too many times, return black
    }

    Intersection in = raycast(origin, direction);
    if (!in.success) { // ray has left the scene
        if (depth == 0) {
            return background_; // if this is the very first bounce, return the background colour
        }
        else {
            // the ray has already bounced off at least one object, do basic directional lighting calculations
            vec3 colour = vec3(0., 0., 0.);
            for (auto const& light : directional_lights_) {
                colour += light->colour * clamp(direction.dot(light->dir * (-1)));
            }
            return colour;
        }
    }

    // Pick a new ray (assuming phong_diffuse + phong_specular = 1)
    // TODO make it so this doesn't have to hold?
    float r = random01();
    vec3 normal = in.object->normal(in.point);
    vec3 direction2;
    if (r < in.object->phong_diffuse) { // selected ray contributes to diffuse reflection
        float theta = sample_diffuse1();
        float phi = sample_diffuse2();
        direction2 = spherical_to_vector(normal, theta, phi);
    }
    else { // selected ray contributes to specular reflection
        vec3 reflected = normal * (2 * normal.dot(direction.norm())) - direction.norm();
        float alpha = sample_specular1(in.object->phong_exponent);
        float phi = sample_specular2();
        direction2 = spherical_to_vector(reflected, alpha, phi);
    }
    vec3 R = normal * (2 * normal.dot(direction2)) - direction2;
    float contribution = in.object->phong_diffuse * clamp(normal.dot(direction2)) + in.object->phong_specular * clamp(R.dot(direction));

    vec3 incoming_colour = pathtrace(in.point, direction2, depth+1, maxdepth);

    // TODO material emittance

    return incoming_colour * in.object->colour * contribution;
}

vec3 Renderer::get_colour(Intersection &in, vec3 incoming) {
    if (in.success) {
        vec3 colour = vec3(0., 0., 0.);
        for (auto const& light : lights_) {
            colour += light->illumination(in, incoming);
        }
        return colour;
    }
    else {
        return background_;
    }
}

void Renderer::set_colour(CImg<unsigned char> &img, int x, int y, vec3 colour) {
    img(x,y,0) = colour.x > 1.0 ? 255 : round(colour.x * 255);
    img(x,y,1) = colour.y > 1.0 ? 255 : round(colour.y * 255);
    img(x,y,2) = colour.z > 1.0 ? 255 : round(colour.z * 255);
}

void Renderer::render(bool raytrace, int samples, int maxdepth) {
    CImg<unsigned char> output(width_, height_, 1, 3);
    for (int i = 0; i < width_; i++) {
        for (int j = 0; j < height_; j++) {
            float a = w_ * (((float)i - ((float)width_ / 2)) / ((float)width_ / 2));
            float b = (-1.) * h_ * (((float)j - ((float)height_ / 2)) / ((float)height_ / 2));
            vec3 dir = (look_ + left_*a + up_*b).norm();
            if (raytrace) {
                vec3 colour = vec3(0., 0., 0.);
                for (int i = 0; i < samples; i++) {
                    colour += pathtrace(camera_, dir, 0, maxdepth);
                }
                colour /= samples;
                set_colour(output, i, j, colour);
            }
            else {
                Intersection in = raycast(camera_, dir);
                set_colour(output, i, j, get_colour(in, camera_));
            }
        }
    }
    //CImgDisplay main_disp(output, "Render");
    output.save("render.png");
    //getchar();
}