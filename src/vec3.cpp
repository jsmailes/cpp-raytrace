// Adapted from https://gist.github.com/rishav007/accaf10c7aa06135d34ddf3919ebdb3b

#include "vec3.h"

vec3::vec3(float x1, float y1, float z1, bool from_rgb) {
    if (from_rgb) {
        x = x1 / 255;
        y = y1 / 255;
        z = z1 / 255;
    }
    else {
        x = x1;
        y = y1;
        z = z1;
    }
}

vec3::vec3(const vec3 &vec) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
}

vec3 vec3::operator+(const vec3 &vec) {
    return vec3(x+vec.x, y+vec.y, z+vec.z);
}

vec3 &vec3::operator+=(const vec3 &vec) {
    x += vec.x;
    y += vec.y;
    z += vec.z;
    return *this;
}

vec3 vec3::operator-(const vec3 &vec) {
    return vec3(x-vec.x, y-vec.y, z-vec.z);
}

vec3 &vec3::operator-=(const vec3 &vec) {
    x -= vec.x;
    y -= vec.y;
    z -= vec.z;
    return *this;
}

vec3 vec3::operator*(const vec3 &vec) {
    return vec3(x*vec.x, y*vec.y, z*vec.z);
}

vec3 vec3::operator*(float val) {
    return vec3(x*val, y*val, z*val);
}

vec3 &vec3::operator*=(float val) {
    x *= val;
    y *= val;
    z *= val;
    return *this;
}

vec3 vec3::operator/(float val) {
    assert(val != 0);
    return vec3(x/val, y/val, z/val);
}

vec3 &vec3::operator/=(float val) {
    assert(val != 0);
    x /= val;
    y /= val;
    z /= val;
    return *this;
}

vec3 &vec3::operator=(const vec3 &vec) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
    return *this;
}

float vec3::dot(const vec3 &vec) {
    return vec.x * x + vec.y * y + vec.z * z;
}

vec3 vec3::cross(const vec3 &vec) {
    float ni = y * vec.z - z * vec.y;
    float nj = z * vec.x - x * vec.z;
    float nk = x * vec.y - y * vec.x;
    return vec3(ni,nj,nk);
}

float vec3::magnitude() {
    return sqrt(magnitude_squared());
}

float vec3::magnitude_squared() {
    return x * x + y * y + z * z;
}

vec3 vec3::norm() {
    assert(magnitude() != 0);
    *this /= magnitude();
    return *this;
}

float vec3::distance(const vec3 &vec) {
    vec3 dist = *this - vec;
    return dist.magnitude();
}
