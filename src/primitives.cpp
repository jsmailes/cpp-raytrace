#include "primitives.h"

Sphere::Sphere(const vec3 origin, float radius, const vec3 colour_) {
    origin_ = origin;
    radius_ = radius;
    colour = colour_;
}

float Sphere::intersect(vec3 origin, vec3 direction) {
    vec3 L = origin_ - origin;
    float tc = L.dot(direction);

    if (tc < 0) {
        return -1.0;
    }

    float d2 = L.magnitude_squared() - (tc * tc);
    float radius2 = radius_ * radius_;

    if (d2 > radius2) {
        return -1.0;
    }

    float t1c = sqrt(radius2 - d2);
    float t1 = tc - t1c;
    // t2 = tc + t1c;

    return t1;
}

vec3 Sphere::normal(vec3 location) {
    return (location - origin_).norm();
}

Triangle::Triangle(const vec3 p0, const vec3 p1, const vec3 p2, const vec3 colour_) {
    p0_ = p0;
    p1_ = p1;
    p2_ = p2;
    normal_ = (p0_ - p1_).cross(p2_ - p0_).norm(); // TODO should be p1_ - p0_, figure out why this gives backwards triangles
    colour = colour_;
}

float Triangle::intersect(vec3 origin, vec3 direction) {
    vec3 edge1 = p1_ - p0_;
    vec3 edge2 = p2_ - p0_;

    vec3 h = direction.cross(edge2);
    float a = edge1.dot(h);
    if (a > -EPSILON && a < EPSILON)
        return -1;

    float f = 1.0/a;

    vec3 s = origin - p0_;
    float u = f * s.dot(h);
    if (u < 0.0 or u > 1.0)
        return -1;

    vec3 q = s.cross(edge1);
    float v = f * direction.dot(q);
    if (v < 0.0 or u + v > 1.0)
        return -1;

    float t = f * edge2.dot(q);
    if (t > EPSILON && t < 1/EPSILON)
        return t;

    return -1;
}

vec3 Triangle::normal(vec3 location) {
    return normal_;
}