#include <iostream>
#include "vec3.h"
#include "primitives.h"
#include "lights.h"
#include "renderer.h"

int main() {
    int width = 1920;
    int height = 1080;
    vec3 camera = vec3(0,0,0);
    vec3 look = vec3(0,0,1);
    vec3 up = vec3(0,1,0);
    float fov = 1.57; // approximately 90 degrees
    Renderer renderer = Renderer(width, height, camera, look, up, fov, vec3(255,255,255,true));

    Sphere s = Sphere(vec3(0.5, 0.5, 7), 1.2);
    s.phong_diffuse = 0.8;
    s.phong_specular = 0.2;
    renderer.add_primitive(&s);

    Triangle t = Triangle(vec3(-3, 1, 7), vec3(-1, 1, 7), vec3(-3, 4, 9));
    renderer.add_primitive(&t);

    Sphere s2 = Sphere(vec3(0,-11,7), 10, vec3(255,0,0,true));
    renderer.add_primitive(&s2);

    Sphere s3 = Sphere(vec3(15,0,7), 10, vec3(0,255,0,true));
    renderer.add_primitive(&s3);

    /*
    AmbientLight l = AmbientLight(vec3(30,30,30,true));
    renderer.add_light(&l);
    PhongLight l2 = PhongLight(vec3(-10,0,0), vec3(150,0,0,true));
    renderer.add_light(&l2);
    PhongLight l3 = PhongLight(vec3(4,5,0), vec3(25,40,200,true));
    renderer.add_light(&l3);
    */

    DirectionalLight l1 = DirectionalLight(vec3(0.4,-0.4,1), vec3(255,255,255,true));
    renderer.add_directional_light(&l1);

    renderer.render(true, 4, 3);

    return 0;
}
