#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <math.h>
#include "vec3.h"
#include "helpers.h"

#define EPSILON 0.0000001

using namespace std;

class Primitive {
public:
    virtual float intersect(vec3 origin, vec3 direction) = 0; // Returns t1 such that the ray intersects at origin + (t1 * direction)
    virtual vec3 normal(vec3 location) = 0; // Returns the normal at location, negative if no intersection
    float phong_diffuse = 1.0;
    float phong_specular = 0.;
    int phong_exponent = 2;
    vec3 colour;
};

class Sphere : public Primitive {
public:
    Sphere(const vec3 origin, float radius, const vec3 colour_ = vec3(1,1,1));
    float intersect(vec3 origin, vec3 direction);
    vec3 normal(vec3 location);
private:
    vec3 origin_;
    float radius_;
};


class Triangle : public Primitive {
public:
    Triangle(const vec3 p0, const vec3 p1, const vec3 p2, const vec3 colour_ = vec3(1,1,1));
    float intersect(vec3 origin, vec3 direction);
    vec3 normal(vec3 location);
private:
    vec3 p0_, p1_, p2_;
    vec3 normal_;
};

class Intersection {
public:
    Primitive *object;
    float t;
    vec3 point;
    bool success;
};

#endif
