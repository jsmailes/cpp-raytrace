#ifndef LIGHT_H
#define LIGHT_H

#include <math.h>
#include "vec3.h"
#include "primitives.h"
#include "helpers.h"

using namespace std;

class Light {
public:
    virtual vec3 illumination(Intersection &in, vec3 &origin) = 0; // Returns illumination on a given object at a given location
};

class AmbientLight : public Light {
public:
    AmbientLight(const vec3 colour);
    vec3 illumination(Intersection &in, vec3 &origin);
private:
    vec3 colour_;
};

class PhongLight : public Light {
public:
    PhongLight(const vec3 pos, const vec3 colour);
    PhongLight(const vec3 pos, const vec3 colour_diffuse, const vec3 colour_specular);
    vec3 illumination(Intersection &in, vec3 &origin);
private:
    vec3 pos_, colour_;
};

class DirectionalLight {
public:
    DirectionalLight(vec3 dir_, const vec3 colour_);
    vec3 dir, colour;
};

#endif
