#ifndef HELPERS_H
#define HELPERS_H

#include <math.h>
#include "vec3.h"

#define EPSILON 0.0000001

using namespace std;

class Colour {
public:
    Colour(unsigned char red = 0, unsigned char green = 0, unsigned char blue = 0);
    Colour(const Colour &col);
    Colour operator+(const Colour &col);
    Colour &operator+=(const Colour &col);

    unsigned char r,g,b;
};

float random01();

vec3 spherical_to_vector(vec3 normal, float theta, float phi);

float sample_diffuse1();
float sample_diffuse2();
float sample_specular1(float exponent);
float sample_specular2();

float clamp(float val);

#endif