#ifndef RENDERER_H
#define RENDERER_H

#include <iostream>

#include <math.h>
#include <bits/stdc++.h>
#include <CImg.h>
#include "primitives.h"
#include "lights.h"
#include "helpers.h"
#include "vec3.h"

using namespace cimg_library;
using namespace std;

class Renderer {
public:
    Renderer(int width, int height, vec3 camera, vec3 look, vec3 up, float fov, vec3 background);
    void add_primitive(Primitive *prim);
    void add_light(Light *light);
    void add_directional_light(DirectionalLight *light);
    vec3 pathtrace(vec3 origin, vec3 direction, int depth, int maxdepth);
    Intersection raycast(vec3 origin, vec3 direction);
    vec3 get_colour(Intersection &in, vec3 incoming);
    void set_colour(CImg<unsigned char> &img, int x, int y, vec3 colour);
    void render(bool raytrace = false, int samples = 1, int maxdepth = 1);
private:
    int width_, height_; // render width and height
    vec3 camera_, look_, up_, left_; // camera's position, direction, up, and left vectors
    float fov_; // camera's field of view, expressed in radians
    vec3 background_; // scene background colour
    list<Primitive*> scene_; // objects in scene
    list<Light*> lights_; // lights in scene
    list<DirectionalLight*> directional_lights_; // directional lights (for raytraced scenes)
    float w_, h_; // values used to calculate ray directions
};

#endif