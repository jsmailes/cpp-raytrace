// Adapted from https://gist.github.com/rishav007/accaf10c7aa06135d34ddf3919ebdb3b

#ifndef VEC3_H
#define VEC3_H

#include <math.h>
#include <assert.h>

using namespace std;

class vec3 {
public:
    float x,y,z;
    vec3(float x1 = 0, float y1 = 0, float z1 = 0, bool from_rgb = false);
    vec3(const vec3 &vec);
    vec3 operator+(const vec3 &vec);
    vec3 &operator+=(const vec3 &vec);
    vec3 operator-(const vec3 &vec);
    vec3 &operator-=(const vec3 &vec);
    vec3 operator*(const vec3 &vec);
    vec3 operator*(float val);
    vec3 &operator*=(float val);
    vec3 operator/(float val);
    vec3 &operator/=(float val);
    vec3 &operator=(const vec3 &vec);
    float dot(const vec3 &vec);
    vec3 cross(const vec3 &vec);
    float magnitude();
    float magnitude_squared();
    vec3 norm();
    float distance(const vec3 &vec);
};

#endif
